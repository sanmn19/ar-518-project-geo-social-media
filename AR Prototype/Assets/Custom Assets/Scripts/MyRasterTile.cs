﻿using System.Linq;
using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Networking;

public class MyRasterTile : MonoBehaviour
{

	Location location;

	[HideInInspector]
	public int longitudeTile;
	[HideInInspector]
	public int latitudeTile;

	public enum TileStyle
    {
		Satellite = 0,
		Street = 1,
		Dark = 2,
		Light = 3
    }

	public TileStyle tileStyle;

	public SpriteRenderer spriteRenderer;

	public Vector2 resolution;

	string[] _mapboxStyles = new string[]
	{
		"styles/v1/mapbox/satellite-v11",
		"styles/v1/mapbox/streets-v11",
		"styles/v1/mapbox/dark-v11",
		"styles/v1/mapbox/light-v11"
	};

	const string comma = ",";

	void Awake()
	{
	}

	public IEnumerator Initialize(Location location, int longitudeTile, int latitudeTile, int zoom)
	{
		this.location = location;
		this.latitudeTile = latitudeTile;
		this.longitudeTile = longitudeTile;
		//string locationString = location.longitude + "," + location.latitude;
		/*string zoomString = "," + zoom;
		double minLongitude = location.longitude - longitudeOffset;
		double maxLongitude = location.longitude + longitudeOffset;
		double minLatitude = location.latitude - latitudeOffset;
		double maxLatitude = location.latitude + latitudeOffset;
		string resolutionString = resolution.x + "x" + resolution.y;*/

		// https://api.mapbox.com/styles/v1/mapbox/streets-v9/tiles/512/1/1/0@2x?access_token=pk.eyJ1Ijoic2FubW4xOSIsImEiOiJja20wcnhhanowYmcwMnBxdDRkNWRscjlyIn0.S_h2VGxL5WrZsRoHrLu0Lg"
		//StartCoroutine(GetRequest("https://api.mapbox.com/styles/v1/mapbox/streets-v11/static/-122.249,37.8655,16,0/256x256?access_token=pk.eyJ1Ijoic2FubW4xOSIsImEiOiJja20wcnhhanowYmcwMnBxdDRkNWRscjlyIn0.S_h2VGxL5WrZsRoHrLu0Lg"));
		//string locationString = "[" + minLongitude + comma + minLatitude + comma + maxLongitude + comma + maxLatitude + "]";
		//string requestString = "https://api.mapbox.com/" + _mapboxStyles[(int)tileStyle] + "/static/" + locationString + zoomString + ",0/256x256?access_token=pk.eyJ1Ijoic2FubW4xOSIsImEiOiJja20wcnhhanowYmcwMnBxdDRkNWRscjlyIn0.S_h2VGxL5WrZsRoHrLu0Lg&attribution=false";
		//string requestString = "https://api.mapbox.com/" + _mapboxStyles[(int)tileStyle] + "/static/" + locationString + "/" + resolutionString + "?access_token=pk.eyJ1Ijoic2FubW4xOSIsImEiOiJja20wcnhhanowYmcwMnBxdDRkNWRscjlyIn0.S_h2VGxL5WrZsRoHrLu0Lg&attribution=false";

		string requestString = "https://api.mapbox.com/" + _mapboxStyles[(int)tileStyle] + "/tiles/" + resolution.x + "/" + zoom + "/" + longitudeTile + "/" + latitudeTile 
			+ "?access_token=pk.eyJ1Ijoic2FubW4xOSIsImEiOiJja20wcnhhanowYmcwMnBxdDRkNWRscjlyIn0.S_h2VGxL5WrZsRoHrLu0Lg&attribution=false";
		//Debug.Log("RequestString " + requestString);

		yield return StartCoroutine(GetRequest(requestString));
	}

	IEnumerator GetRequest(string uri)
	{
		using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
		{
			// Request and wait for the desired page.
			yield return webRequest.SendWebRequest();

			string[] pages = uri.Split('/');
			int page = pages.Length - 1;

			if (webRequest.isNetworkError)
			{
				Debug.LogWarning(pages[page] + ": Error: " + webRequest.error);
			}
			else
			{
				//Debug.Log(pages[page] + ":\nReceived: " + webRequest.downloadHandler.text);
				processAndSetTexture(webRequest);
			}
		}
	}

	void processAndSetTexture(UnityWebRequest webRequest)
    {
		var texture = new Texture2D(256, 256);
		//Debug.Log("Download Handler " + webRequest.downloadHandler.data);
		texture.LoadImage(webRequest.downloadHandler.data);
		spriteRenderer.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
	}

    private void OnTriggerEnter(Collider other)
    {
		MapController.instance.renderTiles(location, transform.position);
		//FirebaseManager.instance.loadSegmentDataBasedOnTile(transform, latitudeTile, longitudeTile, true);
	}

}
