﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileSpawner : MonoBehaviour
{

    public MyRasterTile rasterTile;

    private void OnTriggerEnter(Collider other)
    {
        FirebaseManager.instance.loadSegmentDataBasedOnTile(transform, rasterTile.latitudeTile, rasterTile.longitudeTile, true);
    }

}
