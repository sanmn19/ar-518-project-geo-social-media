﻿using Firebase.Database;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirebaseManager : MonoBehaviour
{

    public static FirebaseManager instance;

    Dictionary<string, string> locationSegments;

    //public Dictionary<string, List<ARObject>> segmentData;
    public Dictionary<string, Dictionary<string, ARObject>> segmentData;

    //DataSnapshot lastDataSnapshot;
    //string lastDataSnapshotKey;

    public const int multiplier = 100000000;

    List<DataSnapshot> pendingParentDataSnapshots;
    List<DataSnapshot> pendingChildrenDataSnapshots;
    List<Vector2> pendingSnapshotGeoLocations;
    List<Transform> pendingSnapshotTileTransforms;

    List<IEnumerator> pendingTileEnumerators;
    Dictionary<string, bool> isTileQueryRunning;

    bool runTileSegmentQueries = true; //Change to per tile later.

    void Awake()
    {
        segmentData = new Dictionary<string, Dictionary<string, ARObject>>();
        locationSegments = new Dictionary<string, string>(128);
        pendingParentDataSnapshots = new List<DataSnapshot>();
        pendingChildrenDataSnapshots = new List<DataSnapshot>();
        pendingSnapshotGeoLocations = new List<Vector2>();
        pendingSnapshotTileTransforms = new List<Transform>();
        pendingTileEnumerators = new List<IEnumerator>();
        isTileQueryRunning = new Dictionary<string, bool>();
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        } else
        {
            Destroy(gameObject);
        }
        loadLocationSegmentData();
    }

    private void Update()
    {
        int i = 0;

        foreach(DataSnapshot snapshot in pendingParentDataSnapshots)
        {
            Vector2 geoLocation = pendingSnapshotGeoLocations[i];
            string key = formatTilePiece((int) geoLocation.x, (int) geoLocation.y);
            if (snapshot != null && snapshot.Children != null)
            {
                Debug.Log("Snapshot processing " + Time.timeSinceLevelLoad);
                foreach (DataSnapshot childSnapshot in snapshot.Children)
                {
                    //Debug.Log("Segment Task " + segment + " with key " + key + " task " + task.Status);
                    //ARObject arObject = JsonUtility.FromJson<ARObject>(childSnapshot.Value.ToString());
                    string keyHash = Animator.StringToHash(key).ToString();
                    parseARObjectChildSnapshot(keyHash, childSnapshot);
                }
                if (snapshot.ChildrenCount != 0)
                {
                    if (AppManager.instance.getAppMode() == 0)
                    {
                        Debug.Log("Spawning children for " + pendingSnapshotGeoLocations[i]);
                        MapController.instance.spawnTileObjects((int)geoLocation.x, (int)geoLocation.y, pendingSnapshotTileTransforms[i]);
                    }
                }
            }
            i++;
        }

        foreach(IEnumerator enumerator in pendingTileEnumerators)
        {
            StartCoroutine(enumerator);
        }

        pendingTileEnumerators = new List<IEnumerator>();
        pendingParentDataSnapshots = new List<DataSnapshot>();
        pendingSnapshotGeoLocations = new List<Vector2>();
        pendingSnapshotTileTransforms = new List<Transform>();
    }

    void parseARObjectChildSnapshot(string key, DataSnapshot snapshot, bool instantiate = false)
    {
        ARObject arObject = convertDictionaryToARObject((IDictionary)snapshot.Value);
        if (!segmentData.ContainsKey(key))
        {
            segmentData[key] = new Dictionary<string, ARObject>(10);
        }
        string arObjectKey = formatLocationKey(new Location(arObject.latitude, arObject.longitude));
        bool containsKey = segmentData[key].ContainsKey(arObjectKey);
        Debug.Log("Parsing: Checking if tileKey " + key + " contains object Key " + arObjectKey);
        if (!containsKey )
        {
            segmentData[key][arObjectKey] = arObject;

            if (instantiate)
            {
                int appMode = AppManager.instance.getAppMode();
                if (appMode == 0)
                {
                    MapController.instance.spawnARObject(arObject, null, key, arObjectKey);
                }
                else
                {
                    ARController.instance.instantiateARObject(arObject, key, arObjectKey);
                }
            }
        } else if(segmentData[key][arObjectKey].timestamp < arObject.timestamp)
        {
            segmentData[key][arObjectKey] = arObject;
            int appMode = AppManager.instance.getAppMode();
            if (appMode == 1)
            {
                ARController.instance.updateARObject(key, arObjectKey);
            } else if(appMode == 0)
            {
                MapController.instance.updateMapARObject(key, arObjectKey);
            }
        }

        Debug.Log("SegmentData: Child " + snapshot.Key + " " + arObject.lookRotation.z);
    }

    void loadLocationSegmentData()
    {
        DatabaseReference databaseReference = FirebaseDatabase.DefaultInstance.GetReference("LocationSegments");
        databaseReference.GetValueAsync().ContinueWith(task => {
            if (task.IsFaulted)
            {
                Debug.LogError("Retrieve failed");
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                Debug.Log(" " + snapshot.Value);
                foreach (DataSnapshot childSnapshot in snapshot.Children)
                {
                    locationSegments[childSnapshot.Key] = childSnapshot.Value.ToString();
                    Debug.Log("child " + childSnapshot.Key + " " + childSnapshot.Value);
                }
                Debug.Log("No children" + snapshot.HasChildren + " child count " + snapshot.ChildrenCount);
            }
        });
        
        databaseReference.ChildAdded += LocationSegmentDataChildAdded;
        databaseReference.ChildRemoved += LocationSegmentDataChildRemoved;
    }

    private void LocationSegmentDataChildRemoved(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        DataSnapshot snapshot = args.Snapshot;
        if (locationSegments.ContainsKey(snapshot.Key))
        {
            locationSegments.Remove(snapshot.Key);
            DatabaseReference databaseReference = FirebaseDatabase.DefaultInstance.GetReference(snapshot.Value.ToString());
            databaseReference.ChildRemoved -= OnTileSegmentChildRemoved;
            databaseReference.ChildAdded -= OnTileSegmentChildAdded;
            databaseReference.ChildChanged -= OnTileSegmentChildChanged;

            //TODO remove all events

            Debug.Log("LocationSegment Event: child Removed" + snapshot.Key + " " + snapshot.Value);
        }
        
    }

    private void LocationSegmentDataChildAdded(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }
        
        DataSnapshot snapshot = args.Snapshot;
        locationSegments[snapshot.Key] = snapshot.Value.ToString();

        Debug.Log("LocationSegment Event: child " + snapshot.Key + " " + snapshot.Value);
    }

    public void loadSegmentDataBasedOnTile(Transform tileTransform, int latitudeTile, int longitudeTile, bool skipLocationCheck = false)
    {
        if (skipLocationCheck)
        {
            string key = formatTilePiece(latitudeTile, longitudeTile); // formatLocationKey(approxLocation);
            Debug.Log("Finding segment with key " + key);

            if (locationSegments.ContainsKey(key))
            {
                string segment = locationSegments[key];
                Debug.Log("Found segment. Retrieving " + segment + " with key " + key);

                if (segmentData.ContainsKey(segment))
                {
                    foreach (ARObject arObject in segmentData[segment].Values)
                    {
                        string arObjectKey = formatLocationKey(new Location(arObject.latitude, arObject.longitude));
                        if (AppManager.instance.getAppMode() == 0)
                        {
                            MapController.instance.spawnARObject(arObject, null, segment, arObjectKey);
                        }
                        else if (AppManager.instance.getAppMode() == 1)
                        {
                            ARController.instance.instantiateARObject(arObject, segment, arObjectKey);
                        }
                    }
                }
                else
                {

                    DatabaseReference databaseReference = FirebaseDatabase.DefaultInstance.GetReference(segment);
                    databaseReference.ChildAdded += OnTileSegmentChildAdded;
                    databaseReference.ChildRemoved += OnTileSegmentChildRemoved;
                    databaseReference.ChildChanged += OnTileSegmentChildChanged;
                    databaseReference.GetValueAsync().ContinueWith(task =>
                    {
                        if (task.IsFaulted)
                        {
                            Debug.LogError("Segment retrieval failed");
                        }
                        else if (task.IsCompleted)
                        {
                            DataSnapshot snapshot = task.Result;
                            pendingParentDataSnapshots.Add(snapshot);
                            pendingSnapshotGeoLocations.Add(new Vector2(latitudeTile, longitudeTile));
                            pendingSnapshotTileTransforms.Add(tileTransform);
                        }
                    });
                    queryTileSegmentForNewARObjects(databaseReference, latitudeTile, longitudeTile, tileTransform);
                }
            }
        }
    }

    void queryTileSegmentForNewARObjects(DatabaseReference parentReference, int latitudeTile, int longitudeTile, Transform tileTransform = null)
    {
        parentReference.GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                Debug.LogError("Segment retrieval failed");
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                pendingParentDataSnapshots.Add(snapshot);
                pendingSnapshotGeoLocations.Add(new Vector2(latitudeTile, longitudeTile));
                pendingSnapshotTileTransforms.Add(tileTransform);
            }

            if (runTileSegmentQueries)
            {
                string key = formatTilePiece(latitudeTile, longitudeTile);
                pendingTileEnumerators.Add(waitAndQuery(3.0f, parentReference, latitudeTile, longitudeTile, tileTransform));
                isTileQueryRunning[key] = true;
                //queryTileSegmentForNewARObjects(parentReference, latitudeTile, longitudeTile, tileTransform);
            }
        });
    }

    IEnumerator waitAndQuery(float waitTime, DatabaseReference parentReference, int latitudeTile, int longitudeTile, Transform tileTransform = null)
    {
        yield return new WaitForSeconds(waitTime);
        Debug.Log("Waiting for " + waitTime + " and Querying");
        string key = formatTilePiece(latitudeTile, longitudeTile);
        if (isTileQueryRunning.ContainsKey(key))
        {
            queryTileSegmentForNewARObjects(parentReference, latitudeTile, longitudeTile, tileTransform);
        }
    }

    public void unloadTileSegment(int latitudeTile, int longitudeTile)
    {
        string key = formatTilePiece(latitudeTile, longitudeTile);
        isTileQueryRunning.Remove(key);
    }

    private void OnTileSegmentChildChanged(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }
    }

    private void OnTileSegmentChildRemoved(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }
    }

    private void OnTileSegmentChildAdded(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        string parentKey = args.Snapshot.Reference.Parent.Key;

        parseARObjectChildSnapshot(parentKey, args.Snapshot, true);
    }

    public void loadSegmentDataBasedOnLocation(Transform tileTransform, Location location, bool skipLocationCheck = false)
    {
        //Location approxLocation = approximateLocation(location);
        int latitude;
        int longitude;
        Helpers.getTileNumberFromLocation(location, AppManager.instance.zoom, out latitude, out longitude);

        loadSegmentDataBasedOnTile(tileTransform, latitude, longitude, skipLocationCheck);
    }

    ARObject convertDictionaryToARObject(IDictionary dictionary)
    {
        IDictionary lookRotationDictionary = dictionary["lookRotation"] as IDictionary;
        /*foreach (string key in dictionary.Keys)
        {
            Debug.Log("Key " + key + " value " + dictionary[key]);
            if(key == "latitude")
            {
                latitude = double.Parse(dictionary[key].ToString());
            } else if(key == "longitude")
            {
                longitude = double.Parse(dictionary[key].ToString());
            } else if(key == "objectType")
            {
                objectType = int.Parse(dictionary[key].ToString());
            }
            if (dictionary[key] is IDictionary)
            {
                foreach (string key2 in lookRotationDictionary.Keys)
                {
                    Debug.Log("Key " + key2 + " value " + lookRotationDictionary[key2]);
                }
            }
        }*/
        return new ARObject(double.Parse(dictionary["latitude"].ToString()),
            double.Parse(dictionary["longitude"].ToString()),
            int.Parse(dictionary["objectType"].ToString()), 
            new Vector3(
                    float.Parse(lookRotationDictionary["x"].ToString()),
                    float.Parse(lookRotationDictionary["y"].ToString()),
                    float.Parse(lookRotationDictionary["z"].ToString())
                    ),
            dictionary["originalUserID"].ToString(),
            int.Parse(dictionary["likes"].ToString()),
            dictionary["data"].ToString()
            );
    }

    public void saveObject(ARObject arObject, out string tileKeyOut, out string arObjectKey)
    {
        Location location = new Location(arObject.latitude, arObject.longitude);
        string arObjectJson = JsonUtility.ToJson(arObject);

        string key = formatLocationKey(location);
        int latitudeTile;
        int longitudeTile;

        Helpers.getTileNumberFromLocation(location, AppManager.instance.zoom, out latitudeTile, out longitudeTile);
        string tileKey = formatTilePiece(latitudeTile, longitudeTile);

        string approxLocationDatabaseKey = Animator.StringToHash(tileKey).ToString();

        locationSegments[tileKey] = approxLocationDatabaseKey;
        FirebaseDatabase.DefaultInstance.GetReference("LocationSegments").Child(tileKey).SetValueAsync(approxLocationDatabaseKey);
        FirebaseDatabase.DefaultInstance.RootReference.Child(approxLocationDatabaseKey).Child(key).SetRawJsonValueAsync(arObjectJson);
        if (!segmentData.ContainsKey(approxLocationDatabaseKey))
        {
            segmentData[approxLocationDatabaseKey] = new Dictionary<string, ARObject>();
        }
        segmentData[approxLocationDatabaseKey][key] = arObject;
        tileKeyOut = approxLocationDatabaseKey;
        arObjectKey = key;

        Debug.Log("Saving with tileKey " + tileKey + " object Key " + key);
        //Debug.Log("databaseReference " + (databaseReference == null) + " " + databaseReference);
    }

    public void saveObject(Location location, int objectType, Vector3 direction) 
    {
        ARObject arObject = new ARObject(location.latitude, location.longitude, objectType, direction, AppManager.instance.username, 0);
        string tileKey;
        string arObjectKey;
        saveObject(arObject, out tileKey, out arObjectKey);
    }

    public string formatTilePiece(int latitudeTile, int longitudeTile)
    {
        return latitudeTile + "," + longitudeTile;
    }

    public string formatLocationKey(Location location)
    {
        return formatLocationPiece(location.latitude) + "," + formatLocationPiece(location.longitude + 180);    //Adding 180 so that firebase autosorts the locations
    }

    public string formatLocationPiece(double locationPiece)
    {
        return locationPiece.ToString("0.00000000").Replace(".", "");
    }

    Location getLocationFromKey(string key)
    {
        string[] locationKeys = key.Split(',');
        return new Location(processkeyPiece(locationKeys[0]), processkeyPiece(locationKeys[1]) - 180);
    }

    double processkeyPiece(string keyPiece)
    {
        string reformattedKeyString = keyPiece.Insert(keyPiece[keyPiece.Length - 1 - 7], ".");
        return double.Parse(reformattedKeyString);
    }

}
