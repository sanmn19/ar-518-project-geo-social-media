﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class ARController : MonoBehaviour
{

    public static ARController instance;

    public GameObject arParentPrefab;

    public GameObject arCircleSpritePrefab;

    public GameObject arCanvas;

    new public Light light;

    public float arObjectSpawnHeight = -1.3f;

    public Location worldOriginalLocation { get; private set; }

    [HideInInspector]
    public List<GameObject> spawnedObjects;
    
    [HideInInspector]
    public GameObject spawnedObject;

    [HideInInspector]
    public ARCameraManager arCameraManager;

    [HideInInspector]
    public bool cursorLocked = false;

    //public ARObject currentArObjectInPointer;
    public string currentTileKey;
    public string currentArObjectKey;

    [HideInInspector]
    public ARPointer arPointer;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            spawnedObjects = new List<GameObject>();
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public Vector3 transformLocationToWorldPos(Location location)
    {
        return transformLocationToWorldPos(location.latitude, location.longitude);
    }

    public Vector3 transformLocationToWorldPos(double latitude, double longitude)
    {
        double conversionFactor = getConversionFactor(latitude);
        return (OrientationReader.instance.adjustedTransform.right * (float)((longitude - worldOriginalLocation.longitude) / conversionFactor))
            + (OrientationReader.instance.adjustedTransform.forward * (float)((latitude - worldOriginalLocation.latitude) / conversionFactor))
            + (Vector3.up * arObjectSpawnHeight); 
        // new Vector3((float)(location.longitude - worldOriginalLocation.longitude), 0, (float)(location.latitude - worldOriginalLocation.latitude));
    }

    double getConversionFactor(double latitude)
    {
        return (0.00001177654688d * Math.Cos(latitude * Mathf.Deg2Rad));
    }

    public void setLightState(bool state)
    {
        light.enabled = state;
    }

    public Location transformWorldPosToLocation(Vector3 worldPosition, double currentLatitude)
    {
        double conversionFactor = getConversionFactor(currentLatitude);
        Location location = new Location((worldPosition.z * conversionFactor) + worldOriginalLocation.latitude, 
            (worldPosition.x * conversionFactor) + worldOriginalLocation.longitude);
        return location;
    }

    public void initializeARMode()
    {
        worldOriginalLocation = LocationReader.instance.getLastLocation();
        checkAndSpawnTileObjects(worldOriginalLocation);
        Instantiate(arParentPrefab);
        ARInitializer.instance.setSessionOriginOrientation(MapController.instance.gyroToUnity(Input.gyro.attitude));
    }

    public void exitARMode()
    {
        ARInitializer.destroy();
    }

    public void checkAndSpawnTileObjects(Location location)
    {
        int latitudeTile;
        int longitudeTile;
        Helpers.getTileNumberFromLocation(location, AppManager.instance.zoom, out latitudeTile, out longitudeTile);
        spawnTileObjects(latitudeTile, longitudeTile);
    }

    public void addARObject(int objectType)
    {
        Location location = LocationReader.instance.getLastLocation();
        Vector3 objectPosition = transformLocationToWorldPos(location);
        Instantiate(ARObjectHolder.instance.arObjectPrefabs[objectType], objectPosition, OrientationReader.instance.adjustedOrientation);
        FirebaseManager.instance.saveObject(location, objectType, Vector3.forward);
    }

    public void spawnTileObjects(int latitude, int longitude)
    {
        Dictionary<string, Dictionary<string, ARObject>> segmentData = FirebaseManager.instance.segmentData;

        string key = FirebaseManager.instance.formatTilePiece(latitude, longitude);
        string keyHash = Animator.StringToHash(key).ToString();
        Debug.Log("Spawning object for tile " + latitude + " " + longitude + " containsKey " + keyHash + " = " + segmentData.ContainsKey(keyHash));
        if (segmentData.ContainsKey(keyHash))
        {
            foreach (ARObject arObject in segmentData[keyHash].Values)
            {
                string arObjectKey = FirebaseManager.instance.formatLocationKey(new Location(arObject.latitude, arObject.longitude));
                instantiateARObject(arObject, keyHash, arObjectKey);
            }
        }
    }

    public void instantiateARObject(ARObject arObject, string tileKey, string arObjectKey)
    {
        //Vector3? worldPosition = LocationReader.instance.transformLocationToWorldPos(arObject.latitude, arObject.longitude);
        Vector3 worldPosition = transformLocationToWorldPos(new Location(arObject.latitude, arObject.longitude));
        // LocationReader.instance.transformLocationToWorldPos(arObject.latitude, arObject.longitude);
        GameObject arGameObject = Instantiate(ARObjectHolder.instance.arObjectPrefabs[arObject.objectType], worldPosition,
            OrientationReader.instance.adjustedOrientation);
        ArObjectDataHolder arObjectDataHolder = arGameObject.GetComponent(typeof(ArObjectDataHolder)) as ArObjectDataHolder;
        arObjectDataHolder.setKeys(tileKey, arObjectKey);
        arObjectDataHolder.refreshSize();
        //arGameObject.transform.localScale = getObjectScaleBasedOnLikes(arObject.likes);
        spawnedObjects.Add(arGameObject);
        Debug.Log("ARMode: Gameobject instantiated " + arGameObject.name + " at " + worldPosition + " with orientation " + OrientationReader.instance.adjustedOrientation);
    }

    public Vector3 getObjectScaleBasedOnLikes(int likes)
    {
        return (Mathf.Pow(1.06f, likes)) * Vector3.one; //(1 + (-10 * likes * (likes - 2))) * Vector3.one;
    }

    public void updateARObject(string tileKey, string arObjectKey)
    {
        if(instance.currentArObjectKey == arObjectKey && currentTileKey == tileKey)
        {
            arPointer.uiSpawner.refreshCurrentMessagePanel();
        }
        if (ArObjectDataHolder.arObjectDataHolderDict != null && ArObjectDataHolder.arObjectDataHolderDict.ContainsKey(tileKey))
        {
            if (ArObjectDataHolder.arObjectDataHolderDict[tileKey].ContainsKey(arObjectKey))
            {
                ArObjectDataHolder.arObjectDataHolderDict[tileKey][arObjectKey].refreshSize();
            }
        }
        //ArObjectDataHolder.arObjectDataHolderDict[tileKey][arObjectKey].
    }

    public void destroyAllObjects()
    {
        foreach(GameObject spawnedObject in spawnedObjects)
        {
            Destroy(spawnedObject);
        }
    }

}
