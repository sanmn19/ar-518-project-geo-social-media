﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;

public class LocationReader : MonoBehaviour
{

    public static LocationReader instance;

    public delegate void OnLocationUpdated(Location location);
    public static OnLocationUpdated onLocationUpdated;

    public Location averageLocation { get; private set; }

    public Location averagePreviousLocation { get; private set; }

    public bool isComputingLocation { get; private set; }

    Queue<Location> previousLocations;

    double latitudeSum;
    double longitudeSum;

    Location removedLocation;

    public Location worldOriginalLocation { get; private set; }
   

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            previousLocations = new Queue<Location>(20);

            if (!Permission.HasUserAuthorizedPermission(Permission.FineLocation))
            {
                Permission.RequestUserPermission(Permission.FineLocation);
            }

            averageLocation = new Location(0, 0);
        } else
        {
            Destroy(gameObject);
        }
    }

    public void startLocationReading()
    {
        StartCoroutine(StartLocation());
    }

    public void stopLocationReading()
    {
        StopCoroutine(StartLocation());
    }

    IEnumerator StartLocation()
    {
        // First, check if user has location service enabled
        #if UNITY_EDITOR
        worldOriginalLocation = new Location(40.4543158f, -86.9236363f);
        while (true) {
            computeAverageLocation(40.4543158f, -86.9236363f);
            if (onLocationUpdated != null)
            {
                onLocationUpdated(getLastLocation());
            }
            yield return null;
        }
#else
        if (!Input.location.isEnabledByUser)
        {
            Debug.Log("Location disabled by user");

            yield break;
        }
        

        // Start service before querying location
        Input.location.Start(1, 2);
        NativeToolkit.StartLocation();

        // Wait until service initializes
        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
            print("Timed out");
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            print("Unable to determine device location");
            yield break;
        }
        else
        {
            yield return new WaitForSeconds(2);
            while (Input.location.status == LocationServiceStatus.Running)
            {
                // Access granted and location value could be retrieved
                yield return null;
                //print("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude);

                //print("Accurate Location: " + NativeToolkit.GetLatitude() + " " + NativeToolkit.GetLongitude());

                //computeAverageLocation(NativeToolkit.GetLatitude(), NativeToolkit.GetLongitude());
                computeAverageLocation(Input.location.lastData.latitude, Input.location.lastData.longitude);
                if(onLocationUpdated != null)
                {
                    onLocationUpdated(getLastLocation());
                }
                if (worldOriginalLocation == null)
                {
                    worldOriginalLocation = new Location(averageLocation.latitude, averageLocation.longitude);
                    //setOriginalOrientation();
                    Debug.Log("World original Location " + worldOriginalLocation.latitude + " " + worldOriginalLocation.longitude);
                    //Debug.Log("Adjusted Direction Vector " + OrientationReader.instance.adjustedDirectionVector);
                }
            }
        }

        // Stop service if there is no need to query location updates continuously
        Input.location.Stop();
        Input.compass.enabled = false;
#endif
    }

    void computeAverageLocation(double latitude, double longitude)
    {
        if (previousLocations.Count > 200)
        {
            removedLocation = previousLocations.Dequeue();
            latitudeSum -= removedLocation.latitude;
            longitudeSum -= removedLocation.longitude;
        }

        latitudeSum += latitude;
        longitudeSum += longitude;

        if (removedLocation == null)
        {
            previousLocations.Enqueue(new Location(latitude, longitude));
            //Debug.Log("Previous locations 1 " + previousLocations);
        } else
        {
            removedLocation.latitude = latitude;    //reusing removedLocation object to prevent garbage collection
            removedLocation.longitude = longitude;
            previousLocations.Enqueue(removedLocation);
            //Debug.Log("Previous locations 2 " + previousLocations);
        }

        if(averagePreviousLocation == null)
        {
            averagePreviousLocation = new Location(0, 0);
            averagePreviousLocation.latitude = latitudeSum / previousLocations.Count;
            averagePreviousLocation.longitude = longitudeSum / previousLocations.Count;
        } else
        {
            averagePreviousLocation.latitude = averageLocation.latitude;
            averagePreviousLocation.longitude = averageLocation.longitude;
        }
        //Debug.Log("previousLocations count " + previousLocations.Count);
        averageLocation.latitude = latitudeSum / previousLocations.Count;
        averageLocation.longitude = longitudeSum / previousLocations.Count;

        isComputingLocation = true;

    }

    public Location getLastLocation()
    {
        return averageLocation;
    }

    public Location getPreviousLocation()
    {
        return averagePreviousLocation == null? new Location(0,0): averagePreviousLocation;
    }

    public Vector3? transformLocationToWorldPos(Location location)
    {
        return transformLocationToWorldPos(location.latitude, location.longitude);
    }

    public Vector3? transformLocationToWorldPos(double latitude, double longitude)
    {
        if (!isComputingLocation)
        {
            return null;
        }
        return (OrientationReader.instance.adjustedTransform.right * (float)((longitude - worldOriginalLocation.longitude) / 0.0000103f))
            + (OrientationReader.instance.adjustedTransform.forward * (float)(latitude - worldOriginalLocation.latitude) / 0.0000103f); // new Vector3((float)(location.longitude - worldOriginalLocation.longitude), 0, (float)(location.latitude - worldOriginalLocation.latitude));
    }

    public Location transformWorldPosToLocation(Vector3 worldPosition)
    {
        Location location = new Location((worldPosition.z * 0.0000103f) + worldOriginalLocation.latitude, (worldPosition.x * 0.0000103f) + worldOriginalLocation.longitude);
        return location;
    }

}
