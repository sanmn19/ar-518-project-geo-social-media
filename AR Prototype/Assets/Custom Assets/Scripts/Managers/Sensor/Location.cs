﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Location
{

    public double latitude;
    public double longitude;

    public Location(double latitude, double longitude)
    {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public bool equals(Location location2)
    {
        return ((this.latitude - location2.longitude) < Mathf.Epsilon) && ((this.longitude - location2.longitude) < Mathf.Epsilon);
    }

    public static bool equals(Location location1, Location location2)
    {
        return ((location1.latitude - location2.longitude) < Mathf.Epsilon) && ((location1.longitude - location2.longitude) < Mathf.Epsilon);
    }

    public static Location minus(Location location1, Location location2)
    {
        return new Location(location1.latitude - location2.latitude, location1.longitude - location2.longitude);
    }

    public override string ToString()
    {
        return "lat= " + latitude + ", long= " + longitude + " ";
    }

}