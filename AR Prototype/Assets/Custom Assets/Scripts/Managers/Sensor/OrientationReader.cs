﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrientationReader : MonoBehaviour
{

    public static OrientationReader instance;

    public GameObject northPrefab;

    Queue<float> previousOrientations;

    float averageOrientation;

    float orientationSum;

    public Quaternion adjustedOrientation { get; private set; }

    public Vector3 adjustedDirectionVector { get; private set; }

    public Transform adjustedTransform { get; private set; }

    float tempRotation;

    void Awake()
    {
        if (instance == null)
        {
            previousOrientations = new Queue<float>(10);
            Input.compass.enabled = true;

            adjustedTransform = Instantiate(northPrefab).transform;
            //adjustedTransform.gameObject.name = "North Transform";
            adjustedDirectionVector = Vector3.forward;
            instance = this;
        }

        setOriginalOrientation();
    }

    void Update()
    {
        computeAverageOrientation(Input.compass.trueHeading);
    }

    void computeAverageOrientation(float orientation)
    {
        if (previousOrientations.Count > 60)
        {
            tempRotation = previousOrientations.Dequeue();
            orientationSum -= tempRotation;
        }

        orientationSum += orientation;

        previousOrientations.Enqueue(orientation);
        //Debug.Log("Previous locations 2 " + previousLocations);

        float count = previousOrientations.Count;
        averageOrientation = orientationSum / count;

        //Debug.Log("previousLocations count " + previousLocations.Count);
    }

    void setOriginalOrientation()
    {
        adjustedOrientation = Quaternion.Euler(0, -Input.compass.trueHeading, 0);
        adjustedDirectionVector = adjustedOrientation * Vector3.forward;
        Debug.LogWarning("Adjusted direction Vector " + adjustedDirectionVector);
        adjustedTransform.rotation = Quaternion.LookRotation(adjustedDirectionVector);
        //originalOrientation = gyroToUnity(Input.gyro.attitude);
    }

    public float getAverageOrientation()
    {
        return averageOrientation;
    }

    public Quaternion getRotation()
    {
        Quaternion orientation = Quaternion.Euler(0, getAverageOrientation(), 0);
        Vector3 directionVector = orientation * Vector3.forward;
        
        return Quaternion.LookRotation(directionVector, Vector3.up);
    }

}
