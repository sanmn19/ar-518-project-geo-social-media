﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour
{

    public static MapController instance;

    public GameObject rasterTilePrefab;

    public GameObject viewInARMessagePrefab;

    public Transform playerTransform;
    public GameObject northSprite;

    public Animator playerAnimator;

    new public Light light;

    public GameObject canvas;

    [HideInInspector]
    public GameObject viewInARMessageObject;

    public Vector2 worldOffset;

    public Dictionary<int, Dictionary<int, MyRasterTile>> rasterTiles;

    [HideInInspector]
    public List<GameObject> spawnedARObjects;

    Location worldCenterGeoLocation;

    //Vector3 originalWorldSpaceLocation;

    double latitudeToWorldSpace;
    double longitudeToWorldSpace;

    Quaternion correctionQuaternion;

    int forwardParamId;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            correctionQuaternion = Quaternion.Euler(90f, 0f, 0f);
            Input.gyro.enabled = true;
            forwardParamId = Animator.StringToHash("Forward");
            rasterTiles = new Dictionary<int, Dictionary<int, MyRasterTile>>();
            spawnedARObjects = new List<GameObject>();
            DontDestroyOnLoad(gameObject);
        } else
        {
            Destroy(gameObject);
        }
    }

    public void startMapLocationUpdate()
    {
        LocationReader.onLocationUpdated += onLocationRead;
    }

    public void stopMapLocationUpdate()
    {
        LocationReader.onLocationUpdated -= onLocationRead;
    }

    void onLocationRead(Location location)
    {


    }

    public void renderTiles(Location location, Vector3 rasterTilePosition)
    {
        StartCoroutine(waitAndRenderTiles(location, rasterTilePosition));
    }

    IEnumerator waitAndRenderTiles(Location location, Vector3 rasterTilePosition)
    {
        //Location approximateLocation = FirebaseManager.instance.approximateLocation(location);
        int longitudeTile = 0;
        int latitudeTile = 0;

        //Debug.Log("Starting Location " + location.latitude + " " + location.longitude);

        Helpers.getTileNumberFromLocation(location, AppManager.instance.zoom, out latitudeTile, out longitudeTile); 

        for (int i = -1; i <= 1; i++)
        {
            for (int j = -1; j <= 1; j++)
            {
                Vector2 tileDirection = new Vector2(i, j);
                yield return StartCoroutine(addNeighbouringTile(tileDirection, worldOffset, rasterTilePosition, longitudeTile, latitudeTile, AppManager.instance.zoom));
            }
        }
    }

    IEnumerator addNeighbouringTile(Vector2 tileDirection, Vector2 worldOffset, Vector3 rasterTilePosition, int longitudeTile, int latitudeTile, int zoom)
    {
        int newLongitudeTile = longitudeTile + (int)tileDirection.x;
        int newLatitudeTile = latitudeTile + (int)tileDirection.y;

        //Location neighbouringLocation = FirebaseManager.instance.getNeighbouringLocation(approxLocation, tileDirection);

        Location neighbouringLocation = Helpers.getLocationFromTileNumber(newLongitudeTile, newLatitudeTile, zoom);

        int longitudeKey = newLongitudeTile;// FirebaseManager.instance.formatLocationPiece(neighbouringLocation.latitude);
        int latitudeKey = newLatitudeTile;// FirebaseManager.instance.formatLocationPiece(neighbouringLocation.longitude);
        
        if (!rasterTiles.ContainsKey(latitudeKey))
        {
            rasterTiles[latitudeKey] = new Dictionary<int, MyRasterTile>();
        }
        if (!rasterTiles[latitudeKey].ContainsKey(longitudeKey))
        {
            GameObject rasterTileGO = Instantiate(rasterTilePrefab);
            rasterTileGO.transform.position = rasterTilePosition + new Vector3(tileDirection.x * worldOffset.x, 0, -tileDirection.y * worldOffset.y);
            rasterTiles[latitudeKey][longitudeKey] = rasterTileGO.GetComponent(typeof(MyRasterTile)) as MyRasterTile;
            MyRasterTile rasterTile = rasterTiles[latitudeKey][longitudeKey];
            //Debug.Log("Tile X and Y " + newTileX + " " + newTileY + " for tile " + tileDirection);
            //Debug.Log("Neighbouring location is " + neighbouringLocation.latitude + " " + neighbouringLocation.longitude + " for tile " + tileDirection);

            yield return StartCoroutine( rasterTile.Initialize(neighbouringLocation, newLongitudeTile, newLatitudeTile, (int)zoom));
        }
    }

    public void setPlayerPinState(bool state)
    {
        playerTransform.gameObject.SetActive(state);
        northSprite.SetActive(state);
    }

    public void destroyMap()
    {
        foreach(int key in rasterTiles.Keys)
        {
            foreach(int longitudeKey in rasterTiles[key].Keys)
            {
                Destroy(rasterTiles[key][longitudeKey].gameObject);
            }
        }

        foreach(GameObject arObject in spawnedARObjects)
        {
            Destroy(arObject);
        }

        spawnedARObjects = new List<GameObject>();

        rasterTiles = new Dictionary<int, Dictionary<int, MyRasterTile>>();
    }

    public void setLightState(bool state)
    {
        light.enabled = state;
    }

    public void initializeMapModeLocation()
    {
        int longitudeTile;
        int latitudeTile;
        Helpers.getTileNumberFromLocation(LocationReader.instance.getLastLocation(), AppManager.instance.zoom, out latitudeTile, out longitudeTile); 
        worldCenterGeoLocation = Helpers.getLocationFromTileNumber(longitudeTile, latitudeTile, AppManager.instance.zoom);

        Debug.Log("Initializing World Center Location " + worldCenterGeoLocation + " player Center " + LocationReader.instance.getLastLocation());

        double scaleInMeters = Helpers.GetTileScaleInMeters((float)worldCenterGeoLocation.latitude, AppManager.instance.zoom) * 2.56f;
        
        int originalLatitudeTile;
        int originalLongitudeTile;
        Helpers.getTileNumberFromLocation(worldCenterGeoLocation, AppManager.instance.zoom, out originalLatitudeTile, out originalLongitudeTile);

        Location longtudeShiftedTile = Helpers.getLocationFromTileNumber(originalLongitudeTile + 1, originalLatitudeTile, AppManager.instance.zoom);
        Location latitudeShiftedTile = Helpers.getLocationFromTileNumber(originalLongitudeTile, originalLatitudeTile + 1, AppManager.instance.zoom);
        double longitudeDifference = longtudeShiftedTile.longitude - worldCenterGeoLocation.longitude;
        double latitudeDifference = latitudeShiftedTile.latitude - worldCenterGeoLocation.latitude;

        longitudeToWorldSpace = scaleInMeters / longitudeDifference;
        longitudeToWorldSpace = longitudeToWorldSpace < 0 ? -longitudeToWorldSpace : longitudeToWorldSpace;

        latitudeToWorldSpace = scaleInMeters / latitudeDifference;
        latitudeToWorldSpace = latitudeToWorldSpace < 0 ? -latitudeToWorldSpace : latitudeToWorldSpace;

        Debug.Log("LongitudeToWorldSpace " + longitudeToWorldSpace + " LatitudeToWorldSpace " + latitudeToWorldSpace);

        playerTransform.position = convertGeoLocationToWorldSpace(LocationReader.instance.getLastLocation());
        playerTransform.rotation = OrientationReader.instance.getRotation();
    }

    public void updatePlayerTransform()
    {
        if (worldCenterGeoLocation != null)
        {
            playerTransform.position = convertGeoLocationToWorldSpace(LocationReader.instance.getLastLocation());
#if !UNITY_EDITOR
            playerTransform.rotation = gyroToUnity(Input.gyro.attitude);// OrientationReader.instance.getRotation();
            Vector3 playerForward = playerTransform.forward;
            Vector3 flatForward = Vector3.ProjectOnPlane(playerForward, Vector3.up);

            //Vector3 fortyFiveForward = Quaternion.Euler(45, 0, 0) * flatForward;
            if (flatForward != Vector3.zero)
            {
                playerTransform.rotation = Quaternion.LookRotation(flatForward);
            }
#endif
        }
    }

    public Quaternion gyroToUnity(Quaternion q)
    {
        return correctionQuaternion * new Quaternion(q.x, q.y, -q.z, -q.w);
    }

    public Location convertWorldSpaceToGeoLocation(Vector3 worldSpace)
    {
        Location returnLocation = convertWorldSpaceToGeoLocationDifference(worldSpace);
        returnLocation.latitude += worldCenterGeoLocation.latitude;
        returnLocation.longitude += worldCenterGeoLocation.longitude;

        return returnLocation;
    }

    public Location convertWorldSpaceToGeoLocationDifference(Vector3 worldSpace)
    {
        Location returnLocation = new Location(worldSpace.z / latitudeToWorldSpace, worldSpace.x / longitudeToWorldSpace);

        return returnLocation;
    }

    public Vector3 convertGeoLocationToWorldSpace(Location location)
    {
        Location locationDifference = Location.minus(location, worldCenterGeoLocation);

        //Debug.Log("Converting To World Space locationDifference " + locationDifference);

        return new Vector3((float)(locationDifference.longitude * longitudeToWorldSpace), 0 , (float)(locationDifference.latitude * latitudeToWorldSpace));

        /*int tileX;
        int tileY;
        getTileNumberFromLocation(worldCenterGeoLocation, (int)zoom, out tileX, out tileY);
        Vector2d centerMercator = Conversions.TileIdToCenterWebMercator(tileX, tileY, (int)zoom);

        Vector2d worldSpace2D = Conversions.GeoToWorldPosition(location.convertToVector2d(), centerMercator, 1f);
        
        Vector3 currentWorldSpacePosition = new Vector3((float)worldSpace2D.x, 0, (float)worldSpace2D.y);
        float scaleInMeters = Conversions.GetTileScaleInMeters((float)location.latitude, (int)zoom) * 256;
        Debug.Log("Tile Scale " + scaleInMeters);

        Debug.Log("Converting location " + location.latitude + " " + location.longitude + " to World Space currenWorldSpacePosition " + currentWorldSpacePosition);

        return currentWorldSpacePosition - originalWorldSpaceLocation;*/
    }

    public void spawnTileObjects(int latitude, int longitude, Transform tileTransform)
    {
        Dictionary<string, Dictionary<string, ARObject>> segmentData = FirebaseManager.instance.segmentData;

        string key = FirebaseManager.instance.formatTilePiece(latitude, longitude);
        string keyHash = Animator.StringToHash(key).ToString();
        Debug.Log("Spawning object for tile " + latitude + " " + longitude + " containsKey " + key + " = " + segmentData.ContainsKey(key));
        if (segmentData.ContainsKey(keyHash))
        {
            foreach (ARObject arObject in segmentData[keyHash].Values)
            {
                string arObjectKey = FirebaseManager.instance.formatLocationKey(new Location(arObject.latitude, arObject.longitude));
                spawnARObject(arObject, tileTransform, keyHash, arObjectKey);
            }
        }
    }

    public void spawnARObject(ARObject arObject, Transform tileTransform, string tileKey, string arObjectKey)
    {
        if (MapObjectDataHolder.mapObjectDataHolderDict == null || !MapObjectDataHolder.mapObjectDataHolderDict.ContainsKey(tileKey) || !MapObjectDataHolder.mapObjectDataHolderDict[tileKey].ContainsKey(arObjectKey))
        {
            //Vector3? worldPosition = LocationReader.instance.transformLocationToWorldPos(arObject.latitude, arObject.longitude);
            Vector3 worldPosition = convertGeoLocationToWorldSpace(new Location(arObject.latitude, arObject.longitude));
            // LocationReader.instance.transformLocationToWorldPos(arObject.latitude, arObject.longitude);
            GameObject mapGameObject = Instantiate(ARObjectHolder.instance.mapModeObjectPrefabs[arObject.objectType], worldPosition, OrientationReader.instance.adjustedOrientation, tileTransform);
            MapObjectDataHolder mapObjectDataHolder = mapGameObject.GetComponent(typeof(MapObjectDataHolder)) as MapObjectDataHolder;
            mapObjectDataHolder.setKeys(tileKey, arObjectKey);
            mapObjectDataHolder.refreshSize();
            spawnedARObjects.Add(mapGameObject);
            Debug.Log("Gameobject instantiated " + mapGameObject.name + " at " + worldPosition + " with orientation " + OrientationReader.instance.adjustedOrientation);
        }
    }

    public Vector3 getObjectScaleBasedOnLikes(int likes)
    {
        return (Mathf.Pow(1.06f, likes)) * Vector3.one;
    }

    public void updateMapARObject(string tileKey, string arObjectKey)
    {
        if (MapObjectDataHolder.mapObjectDataHolderDict != null && MapObjectDataHolder.mapObjectDataHolderDict.ContainsKey(tileKey) && MapObjectDataHolder.mapObjectDataHolderDict[tileKey].ContainsKey(arObjectKey))
        {
            MapObjectDataHolder.mapObjectDataHolderDict[tileKey][arObjectKey].refreshSize();
        }
    }

    public void updatePlayerRunSpeed(float speed)
    {
        playerAnimator.SetFloat(forwardParamId, speed);
    }

}