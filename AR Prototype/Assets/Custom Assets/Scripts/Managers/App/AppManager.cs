﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AppManager : MonoBehaviour
{

    public static AppManager instance;

    public Animator appAnimator;

    public GameObject locationCanvas;
    public Animator maskImageAnimator;

    public GameObject nameCanvasPrefab;

    public List<GameObject> prefabsToInstantiate;

    public static int appModeID;
    public static int appInitializationDoneID;

    [Range(1, 20)]
    public int zoom = 17;

    public string username = "sanmn19";

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            initializeAnimatorIDs();
            instantiatePrefabs();
            DontDestroyOnLoad(gameObject);
        } else
        {
            Destroy(gameObject);
        }
    }

    void initializeAnimatorIDs()
    {
        appModeID = Animator.StringToHash("App Mode");
        appInitializationDoneID = Animator.StringToHash("App Initialization Done");
    }

    public void setAppMode(int appMode)
    {
        appAnimator.SetInteger(appModeID, appMode);
    }

    public int getAppMode()
    {
        return appAnimator.GetInteger(appModeID);
    }

    void instantiatePrefabs()
    {
        foreach(GameObject prefab in prefabsToInstantiate)
        {
            Instantiate(prefab);
        }
    }

    public void startLocationCanvasDeactivation()
    {
        StartCoroutine(deactivateLoadCanvas());
    }

    IEnumerator deactivateLoadCanvas()
    {
        maskImageAnimator.enabled = true;
        yield return new WaitForSeconds(3.0f);
        locationCanvas.SetActive(false);
    }

}