﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class AppPreRunning : StateMachineBehaviour
{

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        LocationReader.instance.startLocationReading();
        AppManager.instance.locationCanvas.SetActive(true);
        LocationReader.onLocationUpdated += onLocationUpdated;
        ARController.instance.arCameraManager = CameraViewManager.instance.arCameraTransform.GetComponent(typeof(ARCameraManager)) as ARCameraManager;
        //FirebaseManager.instance.loadSegmentDataBasedOnLocation(LocationReader.instance.getLastLocation(), true);
    }

    void onLocationUpdated(Location location)
    {
        AppManager.instance.startLocationCanvasDeactivation();
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }

}
