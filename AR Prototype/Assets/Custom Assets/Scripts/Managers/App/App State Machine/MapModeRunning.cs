﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapModeRunning : StateMachineBehaviour
{

    public LayerMask layer;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        CameraViewManager.instance.switchCameraState(CameraViewManager.CameraViewMode.Map);
        MapController.instance.setPlayerPinState(true);
        MapController.instance.startMapLocationUpdate();
        MapController.instance.setLightState(true);
        LocationReader.onLocationUpdated += onLocationUpdated;
        MapController.instance.canvas.SetActive(true);
        CameraViewManager.instance.fadeCamera(true, 0.6f, () =>
        {

        });
    }

    void onLocationUpdated(Location lastLocation)
    {
        MapController.instance.initializeMapModeLocation();
        MapController.instance.renderTiles(LocationReader.instance.getLastLocation(), Vector3.zero);
        LocationReader.onLocationUpdated -= onLocationUpdated;
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        MapController.instance.updatePlayerTransform();
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 screenPosition = Input.mousePosition;
            Ray ray = CameraViewManager.instance.mapCamera.ScreenPointToRay(screenPosition);
            RaycastHit hitInfo;
            Debug.DrawRay(ray.origin, ray.direction * 100, Color.red, 10);
            if (Physics.Raycast(ray, out hitInfo, 200, layer.value, QueryTriggerInteraction.Ignore))
            {
                if (hitInfo.transform.gameObject.layer == 11)
                {
                    Vector3 worldPosition = hitInfo.point;
                    Location locationDifference = MapController.instance.convertWorldSpaceToGeoLocationDifference(worldPosition);
                    Debug.Log("Hit! " + locationDifference);

                    if (locationDifference.latitude > -0.0015f && locationDifference.latitude < 0.0015f
                        && locationDifference.longitude > -0.0015f && locationDifference.longitude < 0.0015f)
                    {
                        if (MapController.instance.viewInARMessageObject == null)
                        {
                            MapController.instance.viewInARMessageObject = Instantiate(MapController.instance.viewInARMessagePrefab);
                            //AppManager.instance.setAppMode(1);
                        }
                        MapController.instance.viewInARMessageObject.transform.position = worldPosition;// + (Vector3.up * 0.25f);
                        Vector3 flatForward = Vector3.ProjectOnPlane(CameraViewManager.instance.mapCameraTransform.forward, Vector3.up);
                        MapController.instance.viewInARMessageObject.transform.rotation = Quaternion.LookRotation(-CameraViewManager.instance.mapCameraTransform.forward);
                    }
                } else if (hitInfo.transform.gameObject.layer == 12)
                {
                    AppManager.instance.setAppMode(1);
                }
            } else
            {
                if (MapController.instance.viewInARMessageObject != null) {
                    Destroy(MapController.instance.viewInARMessageObject);
                    MapController.instance.viewInARMessageObject = null;
                }
            }
        }

#else
        if(Input.touchCount > 0)
        {
            Vector2 screenPosition = Input.touches[0].position;
            Debug.Log("ScreenPosition" + screenPosition);
            Ray ray = CameraViewManager.instance.mapCamera.ScreenPointToRay(screenPosition);
            RaycastHit hitInfo;
            Debug.DrawRay(ray.origin, ray.direction * 100, Color.red, 10);
            if (Physics.Raycast(ray, out hitInfo, 200, layer.value, QueryTriggerInteraction.Ignore))
            {
                if (hitInfo.transform.gameObject.layer == 11)
                {
                    Vector3 worldPosition = hitInfo.point;
                    Location locationDifference = MapController.instance.convertWorldSpaceToGeoLocationDifference(worldPosition);
                    Debug.Log("Hit! " + locationDifference);

                    if (locationDifference.latitude > -0.0015f && locationDifference.latitude < 0.0015f
                        && locationDifference.longitude > -0.0015f && locationDifference.longitude < 0.0015f)
                    {
                        if (MapController.instance.viewInARMessageObject == null)
                        {
                            MapController.instance.viewInARMessageObject = Instantiate(MapController.instance.viewInARMessagePrefab);
                            //AppManager.instance.setAppMode(1);
                        }
                        MapController.instance.viewInARMessageObject.transform.position = worldPosition;// + (Vector3.up * 0.25f);
                        Vector3 flatForward = Vector3.ProjectOnPlane(CameraViewManager.instance.mapCameraTransform.forward, Vector3.up);
                        MapController.instance.viewInARMessageObject.transform.rotation = Quaternion.LookRotation(-CameraViewManager.instance.mapCameraTransform.forward);
                    }
                } else if (hitInfo.transform.gameObject.layer == 12)
                {
                    AppManager.instance.setAppMode(1);
                }
            } else
            {
                if (MapController.instance.viewInARMessageObject != null) {
                    Destroy(MapController.instance.viewInARMessageObject);
                    MapController.instance.viewInARMessageObject = null;
                }
            }
        }
#endif
        Vector3 totalAccel = Input.gyro.userAcceleration - Input.gyro.rotationRate;
        Vector3 acceleration = new Vector3(totalAccel.x < 0? 0: totalAccel.x, 0, totalAccel.z < 0? 0: totalAccel.z) * 5.0f;
        
        float accelMagnitude = 0.0f;
        if(acceleration.magnitude > 0.15f)
        {
            accelMagnitude = 0.5f;
        }
        MapController.instance.updatePlayerRunSpeed(accelMagnitude);
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        CameraViewManager.instance.updatePositionAndRotation = false;
        CameraViewManager.instance.fadeCamera(false, 0.5f, () =>
        {
            Destroy(MapController.instance.viewInARMessageObject);
            MapController.instance.viewInARMessageObject = null;
            MapController.instance.stopMapLocationUpdate();
            MapController.instance.destroyMap();
            MapController.instance.setLightState(false);
            MapController.instance.setPlayerPinState(false);
            MapController.instance.canvas.SetActive(false);
            CameraViewManager.instance.cameraDistance = 5.46f;
            CameraViewManager.instance.updatePositionAndRotation = true;
        });

    }


}