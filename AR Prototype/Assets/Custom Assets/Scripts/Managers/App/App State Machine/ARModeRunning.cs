﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.XR.ARFoundation;

public class ARModeRunning : StateMachineBehaviour
{

    public LayerMask layerMask;

    ARRaycastManager arRaycastManager;

    List<ARRaycastHit> raycastHits = new List<ARRaycastHit>();

    Vector2 screenCenter;

    GameObject spawnedObject;

    ARPointer arPointer;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        LeanTween.delayedCall(1.8f, () => {
            screenCenter = new Vector2(Screen.width / 2, Screen.height / 2);
            ARController.instance.initializeARMode();
            ARInitializer.instance.setNorthPrefabCamera();
            arRaycastManager = ARInitializer.instance.arRaycastManager;
            CameraViewManager.instance.switchCameraState(CameraViewManager.CameraViewMode.AR);
            ARController.instance.spawnedObject = Instantiate(ARController.instance.arCircleSpritePrefab, Vector3.zero, Quaternion.identity);
            spawnedObject = ARController.instance.spawnedObject;
            arPointer = spawnedObject.GetComponent(typeof(ARPointer)) as ARPointer;
            ARController.instance.arPointer = arPointer;
            ARInitializer.instance.arSessionOrigin.enabled = false;
            ARInitializer.instance.arPlaneManager.planesChanged += ArPlaneManager_planesChanged;
            arPointer.setStatus(true);
            spawnedObject.SetActive(true);
            spawnedObject.transform.position = CameraViewManager.instance.arCameraTransform.position + (CameraViewManager.instance.arCameraTransform.forward * 2.3f);
            CameraViewManager.instance.fadeCamera(true, 1.3f, () =>
            {
            });
            ARController.instance.arCameraManager.frameReceived += frameReceived;
            ARController.instance.setLightState(true);
            ARController.instance.arCanvas.SetActive(true);
        });
    }

    private void frameReceived(ARCameraFrameEventArgs args)
    {
        Light mainLight = ARController.instance.light;

        if (args.lightEstimation.averageBrightness.HasValue)
        {
            float? brightness = args.lightEstimation.averageBrightness.Value;
            mainLight.intensity = brightness.Value;
        }
        if (args.lightEstimation.averageColorTemperature.HasValue)
        {
            float? colorTemperature = args.lightEstimation.averageColorTemperature.Value;
            mainLight.colorTemperature = colorTemperature.Value;
        }

        if (args.lightEstimation.colorCorrection.HasValue)
        {
            Color? colorCorrection = args.lightEstimation.colorCorrection.Value;
            mainLight.color = colorCorrection.Value;
        }
        if (args.lightEstimation.mainLightDirection.HasValue)
        {
            Vector3? mainLightDirection = args.lightEstimation.mainLightDirection;
            mainLight.transform.rotation = Quaternion.LookRotation(mainLightDirection.Value);
        }

        if (args.lightEstimation.mainLightColor.HasValue)
        {
            Color? mainLightColor = args.lightEstimation.mainLightColor;

#if PLATFORM_ANDROID
            // ARCore needs to apply energy conservation term (1 / PI) and be placed in gamma
            mainLight.color = mainLightColor.Value / Mathf.PI;
            mainLight.color = mainLight.color.gamma;
#endif
        }

        if (args.lightEstimation.mainLightIntensityLumens.HasValue)
        {
            float? mainLightIntensityLumens = args.lightEstimation.mainLightIntensityLumens;
            mainLight.intensity = args.lightEstimation.averageMainLightBrightness.Value;
        }
        if (args.lightEstimation.ambientSphericalHarmonics.HasValue)
        {
            SphericalHarmonicsL2? sphericalHarmonics = args.lightEstimation.ambientSphericalHarmonics;
            RenderSettings.ambientMode = AmbientMode.Skybox;
            RenderSettings.ambientProbe = sphericalHarmonics.Value;
        }
    }

    private void ArPlaneManager_planesChanged(ARPlanesChangedEventArgs obj)
    {
        if(ARInitializer.instance.arPlaneManager.trackables.count == 0)
        {
            ARInitializer.instance.arSessionOrigin.enabled = false;
            arPointer.setStatus(false);
        } else
        {
            ARInitializer.instance.arSessionOrigin.enabled = true;
            //arPointer.setStatus(true);
        }
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
#if UNITY_EDITOR
        if (Input.GetMouseButton(0))
        {
            Ray ray = CameraViewManager.instance.arCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo, 20, layerMask, QueryTriggerInteraction.Ignore))
            {
                arPointer.setStatus(false);
                ArObjectDataHolder arObjectDataHolder = hitInfo.transform.gameObject.GetComponent(typeof(ArObjectDataHolder)) as ArObjectDataHolder;
                ARController.instance.currentTileKey = arObjectDataHolder.tileKey;
                ARController.instance.currentArObjectKey = arObjectDataHolder.arObjectKey;
                spawnedObject.transform.position = hitInfo.point;
                spawnedObject.transform.rotation = Quaternion.LookRotation(CameraViewManager.instance.arCameraTransform.forward);
                //ARController.instance.currentArObjectInPointer = FirebaseManager.instance.segmentData[arObjectDataHolder.tileKey][arObjectDataHolder.arObjectKey];
            }
        }
#else
        if (!ARController.instance.cursorLocked)
        {
            if (arRaycastManager != null && arRaycastManager.Raycast(screenCenter, raycastHits, UnityEngine.XR.ARSubsystems.TrackableType.PlaneWithinPolygon))
            {
                Ray ray = CameraViewManager.instance.arCamera.ScreenPointToRay(screenCenter);
                RaycastHit hitInfo;
                Debug.DrawRay(ray.origin, ray.direction, Color.magenta, 2);
                if (Physics.Raycast(ray, out hitInfo, 20, layerMask, QueryTriggerInteraction.Ignore))
                {
                    arPointer.setStatus(false);
                    ArObjectDataHolder arObjectDataHolder = hitInfo.transform.gameObject.GetComponent(typeof(ArObjectDataHolder)) as ArObjectDataHolder;
                    ARController.instance.currentTileKey = arObjectDataHolder.tileKey;
                    ARController.instance.currentArObjectKey = arObjectDataHolder.arObjectKey;
                    //ARController.instance.currentArObjectInPointer = FirebaseManager.instance.segmentData[arObjectDataHolder.tileKey][arObjectDataHolder.arObjectKey];
                }
                else
                {
                    arPointer.setStatus(true);
                    //spawnedObject.SetActive(true);
                    
                }
                Pose pose = raycastHits[0].pose;
                spawnedObject.transform.position = pose.position;
                spawnedObject.transform.rotation = pose.rotation;// Quaternion.LookRotation(Vector3.up);
            }
            else
            {
                if (arPointer != null)
                {
                    arPointer.setStatus(null);
                    spawnedObject.transform.position = CameraViewManager.instance.arCameraTransform.position + (CameraViewManager.instance.arCameraTransform.forward * 2.3f);
                    spawnedObject.transform.rotation = Quaternion.LookRotation(Vector3.ProjectOnPlane(CameraViewManager.instance.arCameraTransform.forward, Vector3.up));
                    //spawnedObject.SetActive(false);
                }
            }
        }
#endif
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        CameraViewManager.instance.fadeCamera(false, 0.6f, () =>
        {
            spawnedObject.SetActive(false);
            CameraViewManager.instance.detachARCamera();
            ARController.instance.setLightState(false);
            ARController.instance.exitARMode();
            ARController.instance.destroyAllObjects();
            ARController.instance.arCameraManager.frameReceived -= frameReceived;
            ARController.instance.arCanvas.SetActive(false);
        });
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
