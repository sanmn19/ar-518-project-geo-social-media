﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapView : StateMachineBehaviour
{

    Transform playerTransform;

    Transform mapCameraTransform;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(playerTransform == null)
        {
            playerTransform = MapController.instance.playerTransform;
            mapCameraTransform = CameraViewManager.instance.mapCameraTransform;
        }
        CameraViewManager.instance.arCameraTransform.gameObject.SetActive(false);
        mapCameraTransform.gameObject.SetActive(true);

        updatePosition();
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (CameraViewManager.instance.updatePositionAndRotation)
        {
            updatePosition();
            //updateRotation();
        }
    }

    void updatePosition()
    {
        mapCameraTransform.position =  playerTransform.position + (playerTransform.up * CameraViewManager.instance.cameraDistance) - (playerTransform.forward * CameraViewManager.instance.cameraDistance);
        mapCameraTransform.LookAt(playerTransform);
    }

    void updateRotation()
    {
        playerTransform.rotation = Quaternion.LookRotation(OrientationReader.instance.adjustedDirectionVector);
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }

}