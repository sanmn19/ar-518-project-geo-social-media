﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.UI;

public class CameraViewManager : MonoBehaviour
{

    public enum CameraViewMode
    {
        Map = 0,
        AR = 1
    };

    public static CameraViewManager instance;

    public GameObject mapCameraPrefab;
    public GameObject arCameraPrefab;

    public Image blackoutImage;

    public Transform mapCameraTransform { get; set; }
    public Transform arCameraTransform { get; set; }

    public Camera mapCamera { get; set; }
    public Camera arCamera { get; set; }

    public Animator cameraViewController;

    public float cameraDistance = 10f;

    public bool updatePositionAndRotation = true;

    int cameraViewCodeID;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            initializeAnimatorParameters();
            instantiateCameras();
            DontDestroyOnLoad(gameObject);
        } else
        {
            Destroy(gameObject);
        }
    }

    void initializeAnimatorParameters()
    {
        cameraViewCodeID = Animator.StringToHash("Camera View Code");
    }

    public void switchCameraState(CameraViewMode cameraMode)
    {
        cameraViewController.SetInteger(cameraViewCodeID, (int)cameraMode);
    }

    void instantiateCameras()
    {
        arCameraTransform = Instantiate(arCameraPrefab).transform;
        arCamera = arCameraTransform.gameObject.GetComponent(typeof(Camera)) as Camera;
        mapCameraTransform = Instantiate(mapCameraPrefab).transform;
        mapCamera = mapCameraTransform.gameObject.GetComponent(typeof(Camera)) as Camera;
    }

    public void detachARCamera()
    {
        arCameraTransform.parent = null;
    }

    public void fadeCamera(bool state, float time, System.Action onComplete)
    {
        Color initialColor;
        Color finalColor;
        if(state)
        {
            initialColor = Color.black;
            finalColor = new Color(0, 0, 0, 0);
        }
        else
        {
            initialColor = new Color(0, 0, 0, 0);
            finalColor = Color.black;
        }
        LeanTween.value(0, 1, time).setOnUpdate((val) => {
            blackoutImage.color = Color.Lerp(initialColor, finalColor, val);
        }).setOnComplete(onComplete);
    }

}
