﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARObject
{

    public class ARData
    {
        string userName;
        string data;
        string utcTime;

        public ARData(string userName, string data, string utcTime)
        {
            this.userName = userName;
            this.data = data;
            this.utcTime = utcTime;
        }

        public string getUserName()
        {
            return userName;
        }

        public string getData()
        {
            return data;
        }

        public string getUtcTime()
        {
            DateTime dateTime = DateTime.Parse(utcTime);
            return dateTime.ToLocalTime().ToString();
        }

    }

    [SerializeField]
    public double timestamp;

    public string originalUserID;

    [SerializeField]
    public double latitude;
    public double longitude;

    public int objectType;
    public Vector3 lookRotation;

    public int likes;

    public string data;

    public ARObject(double latitude, double longitude, int objectType, Vector3 lookRotation, string username, int likes, string data = "")
    {
        this.latitude = latitude;
        this.longitude = longitude;
        this.objectType = objectType;
        this.lookRotation = lookRotation;
        this.data = formatData(data, username);
        this.originalUserID = username;
        this.likes = likes;

        refreshTimeStamp();
    }

    void refreshTimeStamp()
    {
        TimeSpan timeSpan = DateTime.UtcNow - new DateTime(1970, 1, 1);
        this.timestamp = timeSpan.TotalSeconds;
    }

    public void appendToData(string reply, string userName)
    {
        reply = reply.Replace(":/", "").Replace(";;", "");
        userName = userName.Replace(":/", "").Replace(";;", "");
        this.data += ":/" + formatData(reply, userName);
        refreshTimeStamp();
    }

    public void updateLikes(int delta)
    {
        likes += delta;
        if (likes < 0)
        {
            likes = 0;
        }

        refreshTimeStamp();
    }

    string formatData(string data, string username)
    {
        return data + ";;" + username + ";;" + DateTime.UtcNow;
    }

    public List<ARData> parseData()
    {
        string[] separator = new string[1] { ":/" };
        string[] separator2 = new string[1] { ";;" };
        string[] messages = data.Split(separator, StringSplitOptions.RemoveEmptyEntries);
        Debug.Log("messages number" + messages.Length);
        if(messages.Length > 0)
        {
            Debug.Log("message" + messages[0]);
        }

        List<ARData> arDataList = new List<ARData>();
        foreach (string message in messages)
        {
            string[] messageSplit = message.Split(separator2, StringSplitOptions.RemoveEmptyEntries);
            Debug.Log("message Split number" + messageSplit.Length);
            if (messages.Length > 0)
            {
                Debug.Log("message" + messageSplit[0]);
            }
            ARData arData = new ARData(messageSplit[1], messageSplit[0], messageSplit[2]);
            arDataList.Add(arData);
        }

        return arDataList;
    }

}