﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARObjectHolder : MonoBehaviour
{

    public static ARObjectHolder instance;

    public List<GameObject> arObjectPrefabs;

    public List<GameObject> mapModeObjectPrefabs;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        } else
        {
            Destroy(gameObject);
        }
    }

}