﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArObjectDataHolder : MonoBehaviour
{

    public static Dictionary<string, Dictionary<string, ArObjectDataHolder>> arObjectDataHolderDict { get; private set; }

    public string tileKey { get; private set; }
    public string arObjectKey { get; private set; }

    public MeshRenderer meshRenderer;
    public bool changeColor = true;

    MaterialPropertyBlock propertyBlock;

    const string colorID = "_Color";

    static Color minimumColor;
    static Color maximumColor;

    void Awake()
    {
        if(arObjectDataHolderDict == null)
        {
            arObjectDataHolderDict = new Dictionary<string, Dictionary<string, ArObjectDataHolder>>();

            minimumColor = new Color(0.1137255f, 0.8901961f, 0.6980392f);
            maximumColor = new Color(0.8901961f, 0.1137255f, 0.2562318f);
        }

        propertyBlock = new MaterialPropertyBlock();
    }

    private void OnDestroy()
    {
        clearDictionary();
    }

    public void setKeys(string tileKey, string arObjectKey)
    {
        this.tileKey = tileKey;
        this.arObjectKey = arObjectKey;

        if (!arObjectDataHolderDict.ContainsKey(tileKey))
        {
            arObjectDataHolderDict[tileKey] = new Dictionary<string, ArObjectDataHolder>();
        }

        arObjectDataHolderDict[tileKey][arObjectKey] = this;
        
    }

    public void refreshColorBasedOnLikes(int likes)
    {
        propertyBlock.SetColor(colorID, Color.Lerp(minimumColor, maximumColor, likes / 30.0f));
        meshRenderer.SetPropertyBlock(propertyBlock);
    }

    public void refreshSize()
    {
        ARObject aRObject = FirebaseManager.instance.segmentData[tileKey][arObjectKey];
        transform.localScale = ARController.instance.getObjectScaleBasedOnLikes(aRObject.likes);

        if (changeColor)
        {
            refreshColorBasedOnLikes(aRObject.likes);
        }
    }

    public void clearDictionary()
    {
        arObjectDataHolderDict.Clear();
    }

}
