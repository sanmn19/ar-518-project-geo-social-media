﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AddARObject : MonoBehaviour
{

    public TMP_InputField inputTextField;
    public Button confirmButton;

    public int objectType = 0;

    void Awake()
    {

    }

    public void OnValueChanged()
    {
        confirmButton.interactable = inputTextField.text.Length > 0;
    }

    public void OnClick()
    {
        if (inputTextField.text.Length > 0)
        {
            /*float locationOffsetX;
            float locationOffsetY;
            processInput(out locationOffsetX, out locationOffsetY);*/

            Location location = LocationReader.instance.getLastLocation();
            //location = new Location(location.latitude + (locationOffsetX), location.longitude + (locationOffsetY));
            //Vector3? objectPosition = LocationReader.instance.transformLocationToWorldPos(location);
            //Vector2d? objectPosition = Conversions.GeoToWorldPosition(location.latitude, location.longitude, new Vector2d(0, 0), 1);

            /*int tileX;
            int tileY;
            MapController.getTileNumberFromLocation(location, (int)MapController.instance.zoom, out tileX, out tileY);*/

            Vector3 spawnPosition = ARController.instance.spawnedObject.transform.position;// + (Vector3.up * ARController.instance.arObjectSpawnHeight);
            Location spawnLocation = ARController.instance.transformWorldPosToLocation(spawnPosition, location.latitude);

            //Vector3 worldPosition = MapController.instance.convertGeoLocationToWorldSpace(location);
            //Vector2dBounds tileBounds = Conversions.TileIdToBounds(tileX, tileY, (int)MapController.instance.zoom);
            //if (objectPosition != null)
            //{
            Debug.Log("Spawn Object at location " + spawnLocation.latitude + " " + spawnLocation.longitude + " world Space Position " + spawnPosition);

            ARObject aRObject = new ARObject(spawnLocation.latitude, spawnLocation.longitude, objectType, -CameraViewManager.instance.arCameraTransform.forward, AppManager.instance.username, 0, inputTextField.text);
            string tileKey;
            string arObjectKey;
            if (AppManager.instance.getAppMode() == 0)
            {
                GameObject arObject = Instantiate(ARObjectHolder.instance.mapModeObjectPrefabs[objectType], spawnPosition, OrientationReader.instance.adjustedOrientation);

                MapController.instance.spawnedARObjects.Add(arObject);
                FirebaseManager.instance.saveObject(aRObject, out tileKey, out arObjectKey);
            }
            else
            {
                GameObject arGameobject = Instantiate(ARObjectHolder.instance.arObjectPrefabs[objectType], spawnPosition, OrientationReader.instance.adjustedOrientation);
                FirebaseManager.instance.saveObject(aRObject, out tileKey, out arObjectKey);
                ArObjectDataHolder arObjectDataHolder = arGameobject.GetComponent(typeof(ArObjectDataHolder)) as ArObjectDataHolder;
                arObjectDataHolder.setKeys(tileKey, arObjectKey);
                arObjectDataHolder.refreshSize();
                ARController.instance.spawnedObjects.Add(arGameobject);
            }

            /*}
            else
            {
                Debug.Log("Spawn Object: location service not initialized");
            }*/
        }
    }

}