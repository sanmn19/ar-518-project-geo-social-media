﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Helpers
{

    public static float GetTileScaleInMeters(float latitude, int zoom)
    {
        return (float)(40075016.685578d * Math.Cos(Mathf.Deg2Rad * latitude) / Math.Pow(2f, zoom + 8));
    }

    public static void getTileNumberFromLocation(Location location, int zoom, out int latitudeTile, out int longitudeTile)
    {
        latitudeTile = lat2tiley(location.latitude, zoom);
        longitudeTile = long2tilex(location.longitude, zoom);
    }

    public static Location getLocationFromTileNumber(int longitudeTile, int latitudeTile, int zoom)
    {
        double longitudeMore = tilex2long(longitudeTile, zoom);
        double longitudeLess = tilex2long(longitudeTile + 1, zoom);
        double latitudeMore = tiley2lat(latitudeTile, zoom);
        double latitudeLess = tiley2lat(latitudeTile + 1, zoom);
        double longitude = (longitudeMore + longitudeLess) / 2;
        double latitude = (latitudeLess + latitudeMore) / 2;

        return new Location(latitude, longitude);
    }

    public static int long2tilex(double lon, int z)
    {
        return (int)(Math.Floor((lon + 180.0) / 360.0 * (1 << z)));
    }

    public static int lat2tiley(double lat, int z)
    {
        return (int)Math.Floor((1 - Math.Log(Math.Tan(lat * Mathf.Deg2Rad) + 1 / Math.Cos(lat * Mathf.Deg2Rad)) / Math.PI) / 2 * (1 << z));
    }

    public static double tilex2long(int x, int z)
    {
        return x / (double)(1 << z) * 360.0 - 180;
    }

    public static double tiley2lat(int y, int z)
    {
        double n = Math.PI - 2.0 * Math.PI * y / (double)(1 << z);
        return 180.0 / Math.PI * Math.Atan(0.5 * (Math.Exp(n) - Math.Exp(-n)));
    }

}
