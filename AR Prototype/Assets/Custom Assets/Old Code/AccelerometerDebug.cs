﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AccelerometerDebug : MonoBehaviour
{

    public Text x;
    public Text y;
    public Text z;

    void Start()
    {
    }

    void Update()
    {
        x.text = Input.gyro.userAcceleration.x.ToString();
        y.text = Input.gyro.userAcceleration.y.ToString();
        z.text = Input.gyro.userAcceleration.z.ToString();
    }

}
