﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationAverageIndicator : MonoBehaviour
{

    Vector3 averagePosition = Vector3.zero;

    void Start()
    {
        transform.position = Vector3.zero;
    }

    private void Update()
    {
        Vector3? transformedLocation = LocationReader.instance.transformLocationToWorldPos(LocationReader.instance.getLastLocation());

        if(transformedLocation != null)
        {
            averagePosition = (averagePosition + transformedLocation.Value)/2;
        }

        transform.position = averagePosition;
    }

}