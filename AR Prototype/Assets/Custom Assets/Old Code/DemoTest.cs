﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoTest : MonoBehaviour
{

    public GameObject prefabCube;

    public double latitude;
    public double longitude;

    void Awake()
    {
        
    }

    public void loadData()
    {
        //FirebaseManager.instance.loadSegmentDataBasedOnLocation(LocationReader.instance.getLastLocation(), true);
    }

    public void spawnObject()
    {
        Location location = LocationReader.instance.getLastLocation();
        Vector3? objectPosition = LocationReader.instance.transformLocationToWorldPos(location);
        if (objectPosition != null)
        {
            Instantiate(prefabCube, objectPosition.Value, OrientationReader.instance.adjustedOrientation);
            FirebaseManager.instance.saveObject(location, 1, Vector3.forward);
        } else
        {
            Debug.Log("Spawn Object: location service not initialized");
        }
    }

    /*public void visualizeSegments()
    {
        Dictionary<string, List<ARObject>> segmentData = FirebaseManager.instance.segmentData;
        Debug.Log("Visualizing segments Count= " + segmentData.Count);
        foreach (string key in segmentData.Keys)
        {
            foreach (ARObject arObject in segmentData[key])
            {
                Vector3? worldPosition = LocationReader.instance.transformLocationToWorldPos(arObject.latitude, arObject.longitude);
                if (worldPosition != null)
                {
                    GameObject gameObject = Instantiate(prefabCube, worldPosition.Value, OrientationReader.instance.adjustedOrientation);
                    Debug.Log("Gameobject instantiated " + prefabCube.name + " at " + worldPosition + " with orientation " + OrientationReader.instance.adjustedOrientation);
                } else
                {
                    Debug.Log("Gameobject instantiation did not happen " + prefabCube.name + " location reader not initialized");
                }
            }
        }
    */

}