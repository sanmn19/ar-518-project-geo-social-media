﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityStandardAssets.Utility;

public class ARInitializer : MonoBehaviour
{

    public static ARInitializer instance;

    public ARSessionOrigin arSessionOrigin;

    public List<GameObject> arObjects;

    public ARPlaneManager arPlaneManager;
    public ARRaycastManager arRaycastManager;
    public ARPointCloudManager arPointCloudManager;
    public ARSession aRSession;
    public ARInputManager arInputManager;

    public FollowTarget northDirection;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }  else
        {
            Destroy(gameObject);
        }
    }

    public static void destroy()
    {
        instance.aRSession.Reset();
        Destroy(instance.gameObject);
        instance = null;
    }

    public void setSessionOriginOrientation(Quaternion orientation)
    {
        arSessionOrigin.transform.rotation = orientation;

        Vector3 forwardVector = arSessionOrigin.transform.forward;
        Vector3 flatForward = Vector3.ProjectOnPlane(forwardVector, Vector3.up);
        Vector3 upVector = arSessionOrigin.trackablesParent.up;
        Vector3 flatUpVector = Vector3.Project(upVector, Vector3.up);

        arSessionOrigin.transform.rotation = Quaternion.LookRotation(flatForward, flatUpVector);
    }

    public void setCamera(Camera arCamera)
    {
        arCamera.transform.parent = arSessionOrigin.transform;
        arCamera.transform.localPosition = Vector3.up * 1.8f;
        arSessionOrigin.camera = arCamera;
    }

    public void setARObjectState(bool state)
    {
        foreach(GameObject ar in arObjects)
        {
            ar.SetActive(state); 
        }
    }

    public void setNorthPrefabCamera()
    {
        northDirection.target = CameraViewManager.instance.arCameraTransform;
    }

    public void setSessionState(bool state)
    {
        arSessionOrigin.enabled = state;
        arPlaneManager.enabled = state;
        arRaycastManager.enabled = state;
        arPointCloudManager.enabled = state;
        aRSession.enabled = state;
        arInputManager.enabled = state;
    }
}
