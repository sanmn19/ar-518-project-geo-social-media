﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GyroDebug : MonoBehaviour
{

    public Text x;
    public Text y;
    public Text z;

    public Text mag;

    void Start()
    {
    }

    void Update()
    {
        x.text = Input.gyro.rotationRate.x.ToString();
        y.text = Input.gyro.rotationRate.y.ToString();
        z.text = Input.gyro.rotationRate.z.ToString();
        mag.text = Input.gyro.rotationRate.magnitude.ToString();
    }

}
