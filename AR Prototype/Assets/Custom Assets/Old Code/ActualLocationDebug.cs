﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ActualLocationDebug : MonoBehaviour
{

    public Text latitude;
    public Text longitude;

    void Start()
    {
        
    }

    void Update()
    {
        Location location = LocationReader.instance.getLastLocation();
        latitude.text = location.latitude.ToString();
        longitude.text = location.longitude.ToString();
    }

}
