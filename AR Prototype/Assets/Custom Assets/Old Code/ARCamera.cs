﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARCamera : MonoBehaviour
{

    public Quaternion correctionQuaternion { get; private set; }

    public float speed = 4;
    public float accelerometerUpdateInterval = 1.0f / 60.0f;
    public float lowPassKernelWidthInSeconds = 1.0f;
    private Vector3 lowPassValue;
    public float lowPassFilterFactor;
    private float pastAccelerationMagnitude;
    private Vector3 movement;
    private float currentAccelerationMagnitude;
    private float accelerationDifference; 

    Rigidbody rb;

    void Awake()
    {
        Input.gyro.enabled = true;
        correctionQuaternion = Quaternion.Euler(90f, 0f, 0f);
        transform.rotation = Quaternion.LookRotation(Vector3.forward);
        transform.position = Vector3.zero;
        lowPassFilterFactor = accelerometerUpdateInterval / lowPassKernelWidthInSeconds;

        rb = GetComponent<Rigidbody>();
        lowPassValue = Input.acceleration;
        pastAccelerationMagnitude = 9.9f;
    }

    void Update()
    {
        //float orientation = OrientationReader.instance.getOrientation();
        
        //Debug.Log("Raw compass val " + Input.compass.trueHeading + " True north " + orientation + " adjustedDirectionVector " + OrientationReader.instance.adjustedDirectionVector);
        //Vector3 lookVector = Quaternion.Euler(0, orientation, 0) * OrientationReader.instance.adjustedDirectionVector;
        
        //transform.rotation = Quaternion.LookRotation(lookVector);
    }

    private void LateUpdate()
    {
        Vector3? transformedLocation = LocationReader.instance.transformLocationToWorldPos(LocationReader.instance.getLastLocation());
        Quaternion attitude = gyroToUnity(Input.gyro.attitude);
        transform.rotation = attitude;
        if (transformedLocation != null) {
            Vector3 userAcceleration = Input.gyro.userAcceleration / Input.gyro.rotationRate.magnitude;
            Vector3 flatRight = Vector3.ProjectOnPlane(transform.right, Vector3.up);
            Vector3 flatForward = Vector3.ProjectOnPlane(transform.forward, Vector3.up);
            float gyroAccelZ = (float)Math.Round((Decimal)userAcceleration.z, 1, MidpointRounding.AwayFromZero);
            float gyroAccelX = (float)Math.Round((Decimal)userAcceleration.x, 1, MidpointRounding.AwayFromZero);
            Debug.Log(gyroAccelZ + " x " + gyroAccelX);
            //transform.position = transform.position + (Input.gyro.userAcceleration.z * flatForward)
                // (Input.gyro.userAcceleration.x * flatRight);// transformedLocation.Value + (Vector3.up * 1.8f);
            //transform.position = new Vector3(transform.position.x, 1.8f, transform.position.z);
        }
    }

    private Quaternion gyroToUnity(Quaternion q)
    {
        return correctionQuaternion * new Quaternion(q.x, q.y, -q.z, -q.w);
    }

    Vector3 LowPassFilterAccelerometer() {
        // Smooths out noise from accelerometer data
        lowPassValue = Vector3.Lerp(lowPassValue, Input.acceleration, lowPassFilterFactor);
        return lowPassValue;
    }

    void FixedUpdate()
    {
        movement = Vector3.zero;
        currentAccelerationMagnitude = LowPassFilterAccelerometer().magnitude;
        accelerationDifference = Mathf.Abs(pastAccelerationMagnitude - currentAccelerationMagnitude);

        // Moves player in the direction of the camera
        if (accelerationDifference > .0015 && .004 > accelerationDifference)
        {
            movement = Camera.main.transform.forward;
        }

        pastAccelerationMagnitude = Mathf.Abs(currentAccelerationMagnitude);

        // Maps the player's real-world steps into in-game movement
        rb.AddForce(movement * speed);
    }

}