﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldSpaceLocationDebug : MonoBehaviour
{

    public Text latitude;
    public Text longitude;

    void Start()
    {

    }

    void Update()
    {
        Vector3? worldSpacePosition = LocationReader.instance.transformLocationToWorldPos( LocationReader.instance.getLastLocation());
        if (worldSpacePosition != null)
        {
            latitude.text = worldSpacePosition.Value.x.ToString();
            longitude.text = worldSpacePosition.Value.z.ToString();
        }
    }

}
