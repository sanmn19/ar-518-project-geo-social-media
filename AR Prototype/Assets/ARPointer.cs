﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ARPointer : MonoBehaviour
{

    public SpriteRenderer circleRenderer;

    public TMP_Text text;

    public UISpawner uiSpawner;

    public Transform rotatingCircle;

    public Vector3 rotateSpeed;

    Coroutine scanRotateRoutine;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void setStatus(bool? active)
    {
        if (active == null)
        {
            circleRenderer.color = Color.grey;
            text.enabled = false;
            rotatingCircle.gameObject.SetActive(true);
            startScanRotationAnimation();
            uiSpawner.readButton.SetActive(false);
            uiSpawner.setReadUiObjectState(false);
        }
        else
        {
            if (active.Value)
            {
                circleRenderer.color = Color.green;
                uiSpawner.readButton.SetActive(false);
                text.enabled = false;
                rotatingCircle.gameObject.SetActive(false);
                if (scanRotateRoutine != null)
                {
                    StopCoroutine(scanRotateRoutine);
                }
                uiSpawner.setReadUiObjectState(false);
            }
            else
            {
                circleRenderer.color = Color.blue;
                uiSpawner.readButton.SetActive(true);
                text.enabled = false;
                rotatingCircle.gameObject.SetActive(false);
                if (scanRotateRoutine != null)
                {
                    StopCoroutine(scanRotateRoutine);
                }
            }
        }
    }

    public void startScanRotationAnimation()
    {
        scanRotateRoutine = StartCoroutine(rotateScanCircle());
    }

    IEnumerator rotateScanCircle()
    {
        while (true)
        {
            rotatingCircle.Rotate(rotateSpeed);
            yield return null;
        }
    }

}
