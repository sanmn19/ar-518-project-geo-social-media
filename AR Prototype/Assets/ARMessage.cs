﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ARMessage : MonoBehaviour
{

    public TMP_Text messageUserText;
    public TMP_Text messageBodyText;
    public TMP_Text messageTimeText;

}
