﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static ARObject;

public class UISpawner : MonoBehaviour
{

    public List<GameObject> uiObjects;
    public List<GameObject> objectTypeButtons;

    public Canvas canvas;

    public List<GameObject> readUiObjects;

    public GameObject readButton;

    public TMP_Text readObjectText;

    public GameObject arMessagePrefab;

    public RectTransform scrollViewContent;
    public ScrollRect scrollRect;
    public RectTransform scrollViewTransform;

    public Button likeButton;
    public TMP_Text likeCountText;

    List<GameObject> arMessageObjects;

    public Sprite likedSprite;
    public Sprite unlikedSprite;

    void OnEnable()
    {
        arMessageObjects = new List<GameObject>();
        if (canvas.worldCamera == null)
        {
            canvas.worldCamera = CameraViewManager.instance.arCamera;
        }
        setUiObjectState(false);
        setObjectTypeButtonsState(false);
    }

    public void OnClick()
    {
        setObjectTypeButtonsState(true);
        setCursorMoveState(true);
    }

    public void OnMessageButtonClick()
    {
        setObjectTypeButtonsState(false);
        setUiObjectState(true);
        //setCursorMoveState(true);
    }

    public void OnDogeButtonClick()
    {
        setObjectTypeButtonsState(false);
        setUiObjectState(false);
        setCursorMoveState(false);
        Location location = LocationReader.instance.getLastLocation();
        Vector3 spawnPosition = ARController.instance.spawnedObject.transform.position;// + (Vector3.up * ARController.instance.arObjectSpawnHeight);
        Location spawnLocation = ARController.instance.transformWorldPosToLocation(spawnPosition, location.latitude);

        ARObject aRObject = new ARObject(spawnLocation.latitude, spawnLocation.longitude, 1, -CameraViewManager.instance.arCameraTransform.forward, AppManager.instance.username, 0, "");
        string tileKey;
        string arObjectKey;
        if (AppManager.instance.getAppMode() == 0)
        {
            GameObject arObject = Instantiate(ARObjectHolder.instance.mapModeObjectPrefabs[1], spawnPosition, OrientationReader.instance.adjustedOrientation);

            MapController.instance.spawnedARObjects.Add(arObject);
            FirebaseManager.instance.saveObject(aRObject, out tileKey, out arObjectKey);
        }
        else
        {
            GameObject arGameobject = Instantiate(ARObjectHolder.instance.arObjectPrefabs[1], spawnPosition, OrientationReader.instance.adjustedOrientation);
            FirebaseManager.instance.saveObject(aRObject, out tileKey, out arObjectKey);
            ArObjectDataHolder arObjectDataHolder = arGameobject.GetComponent(typeof(ArObjectDataHolder)) as ArObjectDataHolder;
            arObjectDataHolder.setKeys(tileKey, arObjectKey);
            arObjectDataHolder.refreshSize();
            ARController.instance.spawnedObjects.Add(arGameobject);
        }
        //setCursorMoveState(true);
    }

    public void setCursorMoveState(bool state)
    {
        ARController.instance.cursorLocked = state;
    }

    public void setObjectTypeButtonsState(bool state)
    {
        foreach (GameObject gameObject in objectTypeButtons)
        {
            gameObject.SetActive(state);
        }
    }

    public void setUiObjectState(bool state)
    {
        foreach(GameObject gameObject in uiObjects)
        {
            gameObject.SetActive(state);
        }
    }

    public void OnReadClicked()
    {
        ARObject aRObject = FirebaseManager.instance.segmentData[ARController.instance.currentTileKey][ARController.instance.currentArObjectKey];
        if (aRObject.objectType == 0)
        {
            destroyScrollContent();
            readButton.SetActive(false);
            loadMessagePanel();

            setReadUiObjectState(true);
            setCursorMoveState(true);

            refreshLikeCount();
            setLikeButtonState(isLiked());
        }
    }

    void refreshLikeCount()
    {
        ARObject aRObject = FirebaseManager.instance.segmentData[ARController.instance.currentTileKey][ARController.instance.currentArObjectKey];
        Debug.Log("Like count " + aRObject.likes);
        likeCountText.text = aRObject.likes.ToString();
        
    }

    bool isLiked()
    {
        return PlayerPrefs.HasKey(ARController.instance.currentArObjectKey);
    }

    void setLikeButtonState(bool state)
    {
        if (state)
        {
            likeButton.image.sprite = likedSprite;
        }
        else
        {
            likeButton.image.sprite = unlikedSprite;
        }
    }

    public void OnReplyClicked()
    {
        if (readObjectText.text.Length != 0)
        {
            ARObject aRObject = FirebaseManager.instance.segmentData[ARController.instance.currentTileKey][ARController.instance.currentArObjectKey];
            aRObject.appendToData(readObjectText.text, AppManager.instance.username);

            GameObject arMessageObject = createARMessageObject();

            ARMessage arMessage = arMessageObject.GetComponent(typeof(ARMessage)) as ARMessage;
            arMessage.messageUserText.text = AppManager.instance.username;
            arMessage.messageBodyText.text = readObjectText.text;
            arMessage.messageTimeText.text = DateTime.UtcNow.ToString();

            readObjectText.text = "";

            string tileKey;
            string arObjectKey;
            FirebaseManager.instance.saveObject(aRObject, out tileKey, out arObjectKey);
        }
    }

    void saveLikeToFirebase(int delta)
    {
        ARObject aRObject = FirebaseManager.instance.segmentData[ARController.instance.currentTileKey][ARController.instance.currentArObjectKey];

        aRObject.updateLikes(delta);
        
        ArObjectDataHolder.arObjectDataHolderDict[ARController.instance.currentTileKey][ARController.instance.currentArObjectKey].refreshSize();

        string tileKey;
        string arObjectKey;

        FirebaseManager.instance.saveObject(aRObject, out tileKey, out arObjectKey);
    }

    GameObject createARMessageObject()
    {
        GameObject arMessageObject = Instantiate(arMessagePrefab);
        arMessageObject.transform.SetParent(scrollViewContent);
        (arMessageObject.GetComponent(typeof(RectTransform)) as RectTransform).anchoredPosition = Vector2.zero;
        arMessageObject.transform.localPosition = new Vector3(arMessageObject.transform.localPosition.x, arMessageObject.transform.localPosition.y, 0);
        arMessageObject.transform.localScale = Vector3.one;
        arMessageObject.transform.localRotation = Quaternion.identity;
        arMessageObjects.Add(arMessageObject);
        //scrollRect.Rebuild(CanvasUpdate.PreRender);
        //LayoutRebuilder.ForceRebuildLayoutImmediate(scrollViewTransform);
        Canvas.ForceUpdateCanvases();

        return arMessageObject;
    }

    public void refreshCurrentMessagePanel()
    {
        destroyScrollContent();
        loadMessagePanel();
    }

    void loadMessagePanel()
    {
        List<ARData> arMessageDataList = FirebaseManager.instance.segmentData[ARController.instance.currentTileKey][ARController.instance.currentArObjectKey].parseData();
        //readObjectText.text = "";// ARController.instance.currentArObjectInPointer.data;
        foreach (ARData arData in arMessageDataList)
        {
            GameObject arMessageObject = createARMessageObject();

            ARMessage arMessage = arMessageObject.GetComponent(typeof(ARMessage)) as ARMessage;
            arMessage.messageUserText.text = arData.getUserName();
            arMessage.messageBodyText.text = arData.getData();
            arMessage.messageTimeText.text = arData.getUtcTime();
        }
    }

    public void OnReadCloseClicked()
    {
        setReadUiObjectState(false);
        readButton.SetActive(true);
        readObjectText.text = "";
        destroyScrollContent();
    }

    void destroyScrollContent()
    {
        foreach(GameObject arMessage in arMessageObjects)
        {
            Destroy(arMessage);
        }

        arMessageObjects.Clear();
    }

    public void setReadUiObjectState(bool state)
    {
        foreach (GameObject gameObject in readUiObjects)
        {
            gameObject.SetActive(state);
        }
    }

    public void OnMessageLiked()
    {
        if (isLiked())
        {
            PlayerPrefs.DeleteKey(ARController.instance.currentArObjectKey);
            saveLikeToFirebase(-1);
        } else
        {
            PlayerPrefs.SetInt(ARController.instance.currentArObjectKey, 1);
            saveLikeToFirebase(1);
        }

        setLikeButtonState(isLiked());
        refreshLikeCount();
        PlayerPrefs.Save();
    }

}
