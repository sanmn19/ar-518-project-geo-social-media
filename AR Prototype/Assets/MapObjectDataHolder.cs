﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapObjectDataHolder : MonoBehaviour
{
    public static Dictionary<string, Dictionary<string, MapObjectDataHolder>> mapObjectDataHolderDict { get; private set; }

    public string tileKey { get; private set; }
    public string arObjectKey { get; private set; }

    public MeshRenderer meshRenderer;

    public bool changeColor = true;

    MaterialPropertyBlock propertyBlock;

    const string colorID = "_Color";

    static Color minimumColor;
    static Color maximumColor;

    void Awake()
    {
        if (mapObjectDataHolderDict == null)
        {
            mapObjectDataHolderDict = new Dictionary<string, Dictionary<string, MapObjectDataHolder>>();
            
            minimumColor = new Color(0.1137255f, 0.8901961f, 0.6980392f, 1);
            maximumColor = new Color(0.8901961f, 0.1137255f, 0.2562318f, 1);
        }

        propertyBlock = new MaterialPropertyBlock();
    }

    private void OnDestroy()
    {
        clearDictionary();
    }

    public void setKeys(string tileKey, string arObjectKey)
    {
        this.tileKey = tileKey;
        this.arObjectKey = arObjectKey;

        if (!mapObjectDataHolderDict.ContainsKey(tileKey))
        {
            mapObjectDataHolderDict[tileKey] = new Dictionary<string, MapObjectDataHolder>();
        }

        mapObjectDataHolderDict[tileKey][arObjectKey] = this;

    }

    void refreshColorBasedOnLikes(int likes)
    {
        Color lerpedColor = Color.Lerp(minimumColor, maximumColor, likes / 30.0f);
        propertyBlock.SetColor(colorID, lerpedColor);
        Debug.Log("Lerped Color " + lerpedColor);
        meshRenderer.SetPropertyBlock(propertyBlock);
    }

    public void refreshSize()
    {
        ARObject aRObject = FirebaseManager.instance.segmentData[tileKey][arObjectKey];
        
        transform.localScale = MapController.instance.getObjectScaleBasedOnLikes(aRObject.likes);

        if (changeColor)
        {
            refreshColorBasedOnLikes(aRObject.likes);
        }
    }

    public void clearDictionary()
    {
        mapObjectDataHolderDict.Clear();
    }

}
