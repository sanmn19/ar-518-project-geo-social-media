﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class NameEntryButton : MonoBehaviour
{

    public TMP_InputField nameInput;

    public Button nextButton;

    public Canvas canvas;

    public void OnClick()
    {
        AppManager.instance.username = nameInput.text;
        PlayerPrefs.SetString("UserName", nameInput.text);
        PlayerPrefs.Save();
        Destroy(canvas.gameObject);
    }

    public void onValueChanged(string newValue)
    {
        nextButton.interactable = nameInput.text.Length > 0;
    }

}
