﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantOrientation : MonoBehaviour
{
    void LateUpdate()
    {
        transform.rotation = Quaternion.LookRotation(Vector3.down, Vector3.forward);
    }
}
