﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollToBottom : MonoBehaviour
{

    public ScrollRect scrollRect;

    void Awake()
    {
    }

    public void onValueChanged(Vector2 contect)
    {
        scrollRect.verticalNormalizedPosition = 0;
    }

}
